import java.util.Comparator;

public class PacketTimeStampComparator implements Comparator<MPacket>{

	@Override
	public int compare(MPacket x, MPacket y) {
		if (x.timeStamp.timeStamp < y.timeStamp.timeStamp)
			return -1;
		else if (x.timeStamp.timeStamp > y.timeStamp.timeStamp)
			return 1;
		else if (x.timeStamp.peerId < y.timeStamp.peerId)
			return -1;
		else if (x.timeStamp.peerId > y.timeStamp.peerId)
			return 1;
		return 0;
	}

}
