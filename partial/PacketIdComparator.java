import java.util.Comparator;

public class PacketIdComparator implements Comparator<MPacket>{

	@Override
	public int compare(MPacket x, MPacket y) {
		if (x.packetID < y.packetID)
			return -1;
		else if (x.packetID > y.packetID)
			return 1;
		return 0;
	}

}
