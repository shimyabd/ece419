import java.io.Serializable;


/**
 * Represents a connection identifier
 */
public class ConnectionID implements Serializable{

	private static final long serialVersionUID = -3376093771682279063L;
	
	public String ipAddress = null;
	public int port = 0;
	
	public ConnectionID(String ip, int port){
		this.ipAddress = ip;
		this.port = port;
	}
	
}
