import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class ClientListenerThread implements Runnable {

    private SocketWrapper socket = null;
    private Hashtable<String, Client> clientTable = null;
    private BlockingQueue<MPacket> eventQueue = null;
    private String myClient = null;
    private int numberOfPlayers = 0;
    private PriorityBlockingQueue<MPacket> executionQ = null;
    private Map<LamportTimeStamp, Integer> goAheadMap = null;

    public ClientListenerThread( SocketWrapper socketW,
                                Hashtable<String, Client> clientTable, BlockingQueue<MPacket> eventQueue, 
                                String client, PriorityBlockingQueue<MPacket> executionQ){
        //this.mSocket = mSocket;
        this.socket = socketW;
        this.clientTable = clientTable;
        this.eventQueue = eventQueue;
        this.myClient = client;
        this.numberOfPlayers = clientTable.size();
        this.executionQ = executionQ;
        this.goAheadMap = new HashMap<>();
        if(Debug.debug) System.out.println("Instatiating ClientListenerThread");
    }

    
    public void run() {
    	MPacket received = null;
    	if(Debug.debug) System.out.println("Starting ClientListenerThread");
    	while(true){
    		try{
    			received = (MPacket) socket.receive(); //- lab 2 replaced this with function call below and incr the counter
    			// received packet could be either event or go_ahead packet
    			if (received.type == MPacket.ACTION){
    				handleReceivedEvent(received);
    			} else if (received.type == MPacket.GO_AHEAD){
    				LamportTimeStamp timeStamp = received.timeStamp;
    				handleGoAhead(timeStamp);
    				
    			} else{
    				throw new UnsupportedOperationException("Undefined packet type!");
    			}
    		} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error in ClientListnerThread: "+e);
			}            
    	}
    }
    
	private Integer incrementAndCountGoAheads(LamportTimeStamp timeStamp) {
		Integer goAheadCount = new Integer(1);
    	if (goAheadMap.containsKey(timeStamp)) {
    		goAheadCount = goAheadMap.get(timeStamp);
    		if (goAheadCount != null){
    			goAheadCount ++;
    		}
    	}
		return goAheadCount;
	}
	
	private void handleGoAhead(LamportTimeStamp timeStamp) throws Exception{
		if (timeStamp.peerId < 0 || timeStamp.timeStamp <0){
			throw new Exception("HandleGoAhead: Lamport TimeStamp error");
		}
		Integer goAheadCount = incrementAndCountGoAheads(timeStamp);
		goAheadMap.put(timeStamp, goAheadCount);
		
		checkExecutionQueue();
	}

	private void checkExecutionQueue() {
		LamportTimeStamp timeStamp;
		MPacket event = executionQ.peek();
		while (event != null){
			timeStamp = event.timeStamp;
			Integer goAheadCount = goAheadMap.get(timeStamp);
			if (goAheadCount!=null && goAheadCount.intValue()== numberOfPlayers-1){

				//This event has made it, remove it from the Q!!
				event = executionQ.poll();
				// make sure it's the same one
				if (event.timeStamp.equals(timeStamp)){
					goAheadMap.remove(timeStamp);
					executeEvent(event);
				} else{
					// something changed, put it back
					executionQ.put(event);
					System.out.println("In handleGoAhead: Event with all acks put back on Q!");
				}
				event = executionQ.peek();
			} else {
				break;
			}
		}
	}


    private void handleReceivedEvent(MPacket eventPacket) throws Exception{
    	int latestTimeStamp = eventPacket.timeStamp.timeStamp;
    	if (latestTimeStamp > LamportClock.getLamportClock()) {
    		LamportClock.setLamportClock(latestTimeStamp);
    	}
    	executionQ.put(eventPacket);
    	MPacket qHead = executionQ.peek();
    	if (qHead == null){
    		System.out.println("Error: Execution Q is empty after put");
    	} else{
    		if (!qHead.goAheadSent){
    			if (!qHead.name.equals(myClient)){
    				// broad cast go ahead
    				MPacket goAheadPacket = new MPacket(MPacket.GO_AHEAD, qHead.event);
    				goAheadPacket.timeStamp.timeStamp = qHead.timeStamp.timeStamp;
    				goAheadPacket.timeStamp.peerId = qHead.timeStamp.peerId;
    				goAheadPacket.name = myClient;
    				eventQueue.put(goAheadPacket);

    				LamportTimeStamp timeStamp = qHead.timeStamp;
    				// add go ahead for this packet
    				handleGoAhead(timeStamp);
    			}
    			qHead.goAheadSent = true;
    		}
    	}
    }

	private void executeEvent(MPacket received) throws UnsupportedOperationException {
		System.out.println("Executing event: " + received);
		Client client = clientTable.get(received.name);
		if(received.event == MPacket.UP){
		    client.forward();
		}else if(received.event == MPacket.DOWN){
		    client.backup();
		}else if(received.event == MPacket.LEFT){
		    client.turnLeft();
		}else if(received.event == MPacket.RIGHT){
		    client.turnRight();
		}else if(received.event == MPacket.FIRE){
		    client.fire();
		}else if(received.event == MPacket.MOVE_PROJECTILE){
		    client.moveProjectile();
		}else{
		    throw new UnsupportedOperationException("In execute event: Undefined event!");
		}
	}
}
