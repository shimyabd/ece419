import java.io.Serializable;

public class LamportTimeStamp implements Serializable{
	private static final long serialVersionUID = 8134908347997046673L;
	
	public int timeStamp = -1;
	public int peerId = -1;
	
	public LamportTimeStamp() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean equals(Object other){
	    if (other == null) return false;
	    if (other == this) return true;
	    if (!(other instanceof LamportTimeStamp))return false;
	    LamportTimeStamp othertimeStamp = (LamportTimeStamp)other;
	    return (this.timeStamp == othertimeStamp.timeStamp &&
	    		this.peerId == othertimeStamp.peerId);
	}
	
	@Override
	public int hashCode() {
		int result = timeStamp;
		result = 31 * result + peerId;
		return result;
	}
}
