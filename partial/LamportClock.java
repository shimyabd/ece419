
public class LamportClock {
	private static int lamportClock = 0;

	public static synchronized void incrementClock(){
		setLamportClock(getLamportClock() + 1);
	}
	
	public static synchronized int getLamportClock() {
		return lamportClock;
	}

	public static synchronized void setLamportClock(int lamportClock) {
		LamportClock.lamportClock = lamportClock;
	}
}
