import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class NamingServer {
	
	//The maximum of clients that will join
	//Server waits until the max number of clients to join 
    private static final int MAX_CLIENTS = 4;
    private int clientCount; //The number of clients before game starts
    private MServerSocket mServerSocket = null;
    private MSocket[] mSocketList = null; //A list of MSockets
    private BlockingQueue eventQueue = null; //A list of events
    
    /*
    * Constructor
    */
    public NamingServer(int port) throws IOException{
        clientCount = 0; 
        mServerSocket = new MServerSocket(port);
        if(Debug.debug) System.out.println("Listening on port: " + port);
        mSocketList = new MSocket[MAX_CLIENTS];
        eventQueue = new LinkedBlockingQueue<MPacket>();
    }
    
    /*
    *Starts the listener and sender threads 
    */
    public void receiveConnections() throws IOException{
        //Listen for new clients
        while(clientCount < MAX_CLIENTS){
            //Start a new listener thread for each new client connection
            MSocket mSocket = mServerSocket.accept();
            
            //replaced with function call
            //new Thread(new ServerListenerThread(mSocket, eventQueue)).start();
            receiveHello(mSocket);
            mSocketList[clientCount] = mSocket;
           
            clientCount++;
        }
        // tell all clients about each other
        handleHello();
        
    }
    
    /*
     * Receives a hello packet from the passed connection and puts it on the shared queue
     */
    private void receiveHello(MSocket mSocket){
    	MPacket received = null;
    	if(Debug.debug) System.out.println("Listening for hello from client");
    	try{
    		received = (MPacket) mSocket.readObject();
    		if(Debug.debug) System.out.println("Received: " + received);
    		eventQueue.put(received);    
    	}catch(InterruptedException e){
    		e.printStackTrace();
    	}catch(IOException e){
    		e.printStackTrace();
    	}catch(ClassNotFoundException e){
    		e.printStackTrace();
    	}
    }
    
    /*
     *Handle the initial joining of players including 
      position initialization - taken from ServerSenderThread
     */
    private void handleHello(){
        //The number of players
        int playerCount = mSocketList.length;
        Random randomGen = null;
        Player[] players = new Player[playerCount];
        if(Debug.debug) System.out.println("In handleHello");
        MPacket hello = null;
        try{        
            for(int i=0; i<playerCount; i++){
                hello = (MPacket)eventQueue.take();
                //Sanity check 
                if(hello.type != MPacket.HELLO){
                    throw new InvalidObjectException("Expecting HELLO Packet");
                }
                if(randomGen == null){
                   randomGen = new Random(hello.mazeSeed); 
                }
                //Get a random location for player
                Point point =
                    new Point(randomGen.nextInt(hello.mazeWidth),
                          randomGen.nextInt(hello.mazeHeight));
                
                //Start them all facing North
                Player player = new Player(hello.name, point, Player.North);
                player.connectionId = new ConnectionID(hello.connectionInfo.ipAddress, hello.connectionInfo.port);
                player.peerId = i;
                players[i] = player;
                
            }
            
            hello.event = MPacket.HELLO_RESP;
            hello.players = players;
            // this tells clients how to connect to each other
            
            //Now broadcast the HELLO
            if(Debug.debug) System.out.println("Sending " + hello);
            for(MSocket mSocket: mSocketList){
                mSocket.writeObject(hello);   
            }
        }catch(InterruptedException e){
            e.printStackTrace();
        }catch(IOException e){
            e.printStackTrace();
        }
    }

        
    /*
    * Entry point for server
    */
    public static void main(String args[]) throws IOException {
        if(Debug.debug) System.out.println("Starting the server");
        int port = Integer.parseInt(args[0]);
        NamingServer server = new NamingServer(port);
                
        server.receiveConnections();    

    }
}
