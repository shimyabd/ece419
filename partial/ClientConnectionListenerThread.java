import java.util.Hashtable;
import java.util.List;

public class ClientConnectionListenerThread implements Runnable {

    private MServerSocket mServerSocket =  null;
    private Hashtable<String, Client> clientTable = null;
    private List<SocketWrapper> socketList;
    private int myPeerId = 0;

    public ClientConnectionListenerThread( MServerSocket mServerSocket,
                                Hashtable<String, Client> clientTable, List<SocketWrapper> socketList, int peerId){
        this.mServerSocket = mServerSocket;
        this.clientTable = clientTable;
        this.socketList = socketList;
        this.myPeerId = peerId;
        if(Debug.debug) System.out.println("Instatiating ClientConnectionListenerThread");
    }
    

    public void run() {
        if(Debug.debug) System.out.println("Starting ClientConnectionListenerThread");
        while(true){
            try{
            	MSocket mSocket = mServerSocket.accept();
            	SocketWrapper socketW = new SocketWrapper(mSocket, myPeerId, -1);
            	socketList.add(socketW);
            	if(Debug.debug) System.out.println("New socket accepted");
            	//Start a new listener thread 
            	//new Thread(new ClientListenerThread(mSocket, clientTable)).start();
            }catch(Exception e){
                e.printStackTrace();
            }            
        }
    }
}
