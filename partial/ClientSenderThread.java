import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;


public class ClientSenderThread implements Runnable {

    private List<SocketWrapper> socketList = null;
    private BlockingQueue<MPacket> eventQueue = null;
    private PriorityBlockingQueue<MPacket> executionQ = null;
    private String myClient = null;
    private int myPeerId;
    
    public ClientSenderThread(List<SocketWrapper> socketList,
                              BlockingQueue eventQueue, PriorityBlockingQueue<MPacket> executionQ, String clientName, int peerId){
        this.socketList = socketList;
        this.eventQueue = eventQueue;
        this.executionQ = executionQ;
        this.myClient = clientName;
        this.myPeerId = peerId;
    }
    
    public void run() {
        MPacket toServer = null;
        if(Debug.debug) System.out.println("Starting ClientSenderThread");
        while(true){
            try{                
                //Take packet from queue
                toServer = (MPacket)eventQueue.take();
                // add to your execution Q
                if (toServer.type == MPacket.ACTION){
                	LamportClock.incrementClock();
                	int timeStamp = LamportClock.getLamportClock();
                	toServer.timeStamp.timeStamp = timeStamp;
                	toServer.timeStamp.peerId = myPeerId;
                	executionQ.put(toServer);
                	//addEvent(toServer);
                }
                
                if(Debug.debug) System.out.println("Broadcasting " + toServer);
                for (SocketWrapper socket: socketList){
                	socket.send(toServer);
                }
            }catch(InterruptedException e){
                e.printStackTrace();
                Thread.currentThread().interrupt();    
            }
            
        }
    }
    
}
