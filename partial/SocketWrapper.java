import java.io.IOException;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This class is a wrapper around Msocket that acts like TCP.
 * Lost packets are resent until acknowledged.
 * peerId and packetId together uniquely identify a packet.
 * sentCounter is used to assign the packetId and this peer's id is peerId
 * Ack carry the same identifier as the packet that was received
 * @author Abdel
 *
 */
public class SocketWrapper {
	private static final long TIMEOUT_PERIOD = 200; //in ms
	private MSocket socket = null;
	private int myPeerId = 0;
	private int remotePeerId = 0;
	// monotonically increasing counter for sent packets
	private int sentCounter = 0;
	// counter for received packets
	private int receivedCounter = 0;
	private PriorityBlockingQueue<MPacket> incomingQ = null;
	private PriorityBlockingQueue<MPacket> ackQ = null;
	
	
	/**
	 * Constructor
	 * @param mSocket
	 */
	public SocketWrapper(MSocket mSocket, int peerID, int remotePeerId){
		this.socket = mSocket;
		this.myPeerId = peerID;
		this.remotePeerId = remotePeerId;
		sentCounter = 0;
		receivedCounter = 0;
		incomingQ = new PriorityBlockingQueue<>(20, new PacketIdComparator());
		ackQ = new PriorityBlockingQueue<>(20, new PacketIdComparator());
	}
	
	/**
	 * @return true if ack has been received
	 */
	private boolean sendAttempt(MPacket packet){
		socket.writeObject(packet);
		//Date startTime = new Date();
		// try to get ack untill timeout
		try{
			while(true){
				//fetchIncomingPacket();
				MPacket ackPacket = ackQ.poll(TIMEOUT_PERIOD, TimeUnit.MILLISECONDS);
				if (ackPacket == null || ackPacket.type != MPacket.ACK || ackPacket.peerID != myPeerId){
					return false;
				} else if (ackPacket.packetID == sentCounter){
					return true;
				} else if (ackPacket.packetID > sentCounter){
					ackQ.offer(ackPacket);
					return false;
				} // else we got an old ack, throw it and try again
			}
		}catch(Exception e){
			System.out.println("Error sending packet: " + e);
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Sends packet and receives ack, resends if no ack received
	 * @param packet
	 */
	public void send(MPacket packet){
		packet.peerID = myPeerId;
		packet.packetID = sentCounter;
		
		while (sendAttempt(packet) != true);
		//sendAttempt(packet);
		sentCounter++;
	}
	
	/**
	 * get incoming packet and send ack to sender
	 * puts fetched packets on their respective queues
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	private void fetchIncomingPacket() throws ClassNotFoundException, IOException{
		// get incoming packet
		MPacket packet;
		packet = (MPacket)socket.readObject();
		if (remotePeerId == -1) {
			remotePeerId = packet.peerID;
		}
		if (packet.type == MPacket.ACK) {
			ackQ.add(packet);
		} else {
			incomingQ.add(packet);

			// send ack to sender
			MPacket ackPacket = new MPacket(MPacket.ACK, 0);
			ackPacket.peerID = packet.peerID;
			ackPacket.packetID = packet.packetID;
			socket.writeObject(ackPacket);
		}
	}
	
	private MPacket receiveAttempt() throws ClassNotFoundException, IOException{
		
		fetchIncomingPacket();
		
		MPacket packet = incomingQ.poll();
		if (packet == null){
			return null;
		}
		// check if it already exists
		if (packet.packetID < receivedCounter){
			// packet already received
			return null;
		} else if (packet.packetID == receivedCounter) {
			// packet in order, increment counter and return new packet
			receivedCounter++;
			return packet;
		} else {
			// packet out of order, put back in queue
			incomingQ.offer(packet);
			return null;
		}
	}
	
	public MPacket receive() throws ClassNotFoundException, IOException{
		MPacket packet = null;
		while (packet == null){
			packet = receiveAttempt();
		}
		return packet;
	}
}
